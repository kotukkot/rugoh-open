<?php
namespace App\Domain\Comlink;

use Exception;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class ComlinkActions
{
    /**
     * Получение метаданных клиента игры
     * @return array
     * @throws Exception
     */
    public function getGameMetaData(): array
    {
        return $this->getData('/metadata')->json();
    }

    /**
     * Получение обозначений из игры
     * @return array
     * @throws Exception
     */
    public function getEnumsData(): array
    {
        return $this->getData('/enums', 'GET')->json();
    }

    /**
     * Получение локализации из игры
     * @param $latestLocalizationBundleVersion - версия последней сборки у локализации (в метаданных игры)
     * @return string
     * @throws Exception
     */
    public function getLocalizationData($latestLocalizationBundleVersion): string
    {
        return $this->getData('/localization', 'POST', [
            'unzip' => true,
            'payload' => [
                'id' => $latestLocalizationBundleVersion,
            ],
            'enums' => false,
        ])->body();
    }

    /**
     * Получение игровых ивентов
     * @return array
     * @throws Exception
     */
    public function getGameEventsData(): array
    {
        return $this->getData('/getEvents', 'POST', [
            'enums' => false,
        ])->json();
    }

    /**
     * Получение информации об аккаунте
     * @param string $playerId
     * @return array
     * @throws Exception
     */
    public function getAccountData(string $playerId): array
    {
        return $this->getData('/player', 'POST', [
            'payload' => [
                'allyCode' => $playerId,
            ],
            'enums' => false,
        ])->json();
    }

    /**
     * Получение данных в Comlink
     * @param string $path
     * @param string $method
     * @param array|null $options
     * @param array|null $headers
     * @return Response
     * @throws Exception
     */
    public function getData(string $path, string $method = 'POST', ?array $options = [], ?array $headers = []): Response
    {
        try {
            $url = env('API_URL') . $path;

            $data = [
                'headers' => [
                    'Authorization' => 'Basic ' . base64_encode(env('API_AUTH_EMAIL') . ':' . env('API_AUTH_PASSWORD')),
                    'accept-encoding' => 'gzip, deflate',
                ],
            ];
            if($method === 'POST' && !empty($options)) {
                $data['json'] = $options;
            }

            return Http::timeout(60)
                ->send($method, $url, $data);
        } catch (Exception $e) {

            Log::channel('comlink')->error($e->getMessage(), ['exception' => $e]);

            throw $e;
        }
    }
}
