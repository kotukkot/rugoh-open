export const sidebarIsClose = (state, data) => {
    state.commit('sidebarIsClose', data);
};

export const isDarkTheme = (state, data) => {
    state.commit('isDarkTheme', data);
};
