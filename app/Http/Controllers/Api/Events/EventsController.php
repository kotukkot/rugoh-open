<?php

namespace App\Http\Controllers\Api\Events;

use App\Domain\Comlink\ComlinkActions;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class EventsController extends Controller
{
    /**
     * Получение игровых ивентов
     */
    public function list()
    {
        return Cache::remember('events', 6000, function () {
            return (new ComlinkActions())->getGameEventsData();
        });

    }
}
