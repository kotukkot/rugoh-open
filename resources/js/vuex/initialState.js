export const initialState = {
    sidebarIsClose: checkValueInLocalStorage('sidebarIsClose', false),
    isDarkTheme: checkValueInLocalStorage('isDarkTheme', false),
};

function checkValueInLocalStorage(key, defaultValue) {
    if(localStorage.getItem(key)) {
        return JSON.parse(localStorage.getItem(key));
    } else {
        return defaultValue;
    }
}
