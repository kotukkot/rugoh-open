<?php

namespace App\Domain\User\Models;

use App\Models\BaseModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Domain\User\Models
*/
class User extends BaseModel
{
    use SoftDeletes, Authenticatable;
    protected $table = 'user';

    protected $fillable = [];

    protected $casts = [

    ];

    protected $hidden = [

    ];

}
