import {createStore} from 'vuex';
import { initialState } from './initialState.js';
import * as mutations from './mutations.js';
import * as actions from './actions.js';
import * as getters from './getters.js';


const state = {...{}, ...initialState};

export default createStore({
    state,
    mutations,
    actions,
    getters
});
