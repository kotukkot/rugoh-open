<?php

namespace App\Http\Controllers\Api;

use App\Domain\Comlink\ComlinkActions;
use App\Domain\Localizations\Actions\LocalizationsActions;
use App\Http\Controllers\Controller;
use http\Env\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Mime\Part\File;

class DeveloperController extends Controller
{
    /**
     * @throws \Exception
     */
    public function test()
    {
//        $test = (new ComlinkActions())->getLocalizationData();
//        $test = (new ComlinkActions())->getEnumsData();
        $test = (new ComlinkActions())->getGameEventsData();
//        $test = (new ComlinkActions())->getGameMetaData();
//        $test = (new ComlinkActions())->getAccountData('222598634');
        dd($test->json());
    }
    public function metadata()
    {
        return (new ComlinkActions())->getGameMetaData();
    }
    public function localization()
    {
        $response = (new ComlinkActions())->getLocalizationData('ofvvmKVaRl2G2eICSbGIxw');
        Storage::disk('local')->put('localizations/localization.json', $response);

        $contents = Storage::disk('local')->get('localizations/localization.json');
        $data = json_decode($contents, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            die('Ошибка при декодировании JSON');
        }

        foreach ($data as $filename => $content) {
            Storage::disk('local')->put('localizations/' . $filename, $content);
        }
    }

    public function getLocalizations()
    {
        $localizations = collect(LocalizationsActions::LOCALES);
        return $localizations->map(function ($value, $key) {
            return [
                'key' => $key,
                'name' => $value['name'],
            ];
        });
    }

    public function getLocalizationValue($localeName, $key)
    {
        return (new LocalizationsActions())->getLocalizationValue($localeName, $key);
    }
}
