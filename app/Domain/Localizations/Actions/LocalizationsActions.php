<?php

namespace App\Domain\Localizations\Actions;

use Illuminate\Support\Facades\Storage;

class LocalizationsActions
{
    /**
     * Доступные локализации
     */
    public const LOCALES = [
        'RUS_RU' => [
            'file' => 'Loc_RUS_RU.txt',
            'name' => 'Русский (Russia)',
            'sort_order' => 1,
        ],
        'ENG_US' => [
            'file' => 'Loc_ENG_US.txt',
            'name' => 'English (United States)',
            'sort_order' => 2,
        ],
        'CHS_CN' => [
            'file' => 'Loc_CHS_CN.txt',
            'name' => 'Simplified Chinese (China)',
            'sort_order' => 3,
        ],
        'CHT_CN' => [
            'file' => 'Loc_CHT_CN.txt',
            'name' => 'Traditional Chinese (China)',
            'sort_order' => 4,
        ],
        'FRE_FR' => [
            'file' => 'Loc_FRE_FR.txt',
            'name' => 'French (France)',
            'sort_order' => 5,
        ],
        'GER_DE' => [
            'file' => 'Loc_GER_DE.txt',
            'name' => 'German (Germany)',
            'sort_order' => 6,
        ],
        'IND_ID' => [
            'file' => 'Loc_IND_ID.txt',
            'name' => 'Indonesian (Indonesia)',
            'sort_order' => 7,
        ],
        'ITA_IT' => [
            'file' => 'Loc_ITA_IT.txt',
            'name' => 'Italian (Italy)',
            'sort_order' => 8,
        ],
        'JPN_JP' => [
            'file' => 'Loc_JPN_JP.txt',
            'name' => 'Japanese (Japan)',
            'sort_order' => 9,
        ],
        'KOR_KR' => [
            'file' => 'Loc_KOR_KR.txt',
            'name' => 'Korean (South Korea)',
            'sort_order' => 10,
        ],
        'POR_BR' => [
            'file' => 'Loc_POR_BR.txt',
            'name' => 'Portuguese (Brazil)',
            'sort_order' => 11,
        ],
        'SPA_XM' => [
            'file' => 'Loc_SPA_XM.txt',
            'name' => 'Spanish (Latin America)',
            'sort_order' => 12,
        ],
        'THA_TH' => [
            'file' => 'Loc_THA_TH.txt',
            'name' => 'Thai (Thailand)',
            'sort_order' => 13,
        ],
        'TUR_TR' => [
            'file' => 'Loc_TUR_TR.txt',
            'name' => 'Turkish (Turkey)',
            'sort_order' => 14,
        ],
    ];

    /**
     * Получение значения локализации по ключу
     *
     * @param string $localeName - Язык локализации (например: RUS_RU)
     * @param string $key - Ключ (например: PLAYERPORTRAIT_GAMGUARD_TITLE)
     * @return string|null
     */
    public function getLocalizationValue($localeName, $key)
    {
        // Формируем имя файла на основе переданного имени локализации
        $fileName = 'localizations/Loc_' . strtoupper($localeName) . '.txt';

        // Проверка существования файла
        if (Storage::disk('local')->exists($fileName)) {
            // Чтение содержимого файла
            $fileContents = Storage::disk('local')->get($fileName);

            // Разбиваем содержимое файла на строки
            $lines = explode("\n", $fileContents);

            // Проход по каждой строке и поиск значения по ключу
            foreach ($lines as $line) {
                // Используем разделитель "|" для разделения ключа и значения
                $parts = explode('|', $line);

                // Проверка, что есть две части (ключ и значение)
                if (count($parts) === 2) {
                    $lineKey = trim($parts[0]);
                    $lineValue = trim($parts[1]);

                    // Сравнение ключа
                    if ($lineKey === $key) {
                        return $lineValue;
                    }
                }
            }
        }

        // Если ключ не найден или файл не существует
        return null;
    }
}
