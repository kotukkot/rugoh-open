import { createApp } from 'vue';

import App from './components/App.vue';

// Стили
import '../scss/main.scss';

import config from './config/config';

const app = createApp(App);

import { router } from "./router.js";
app.use(router);

import store from './vuex/store';
app.use(store);

import axios from 'axios'
axios.defaults.baseURL = config.apiUrl;
app.config.globalProperties.$axios = axios;

app.mount('#app');
