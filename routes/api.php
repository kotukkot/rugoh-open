<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route для разработки и тестов
Route::group([
    'prefix' => '/developers',
], function () {
    Route::get('/', 'DeveloperController@test');
    Route::get('/metadata', 'DeveloperController@metadata');
    Route::get('/localization', 'DeveloperController@localization');
    Route::get('/localization/list', 'DeveloperController@getLocalizations');
    Route::get('/localization/{localeName}/{key}', 'DeveloperController@getLocalizationValue');
});

Route::group([
    'namespace' => 'Events',
    'prefix' => '/events',
], function () {
    Route::get('/', 'EventsController@list');
});

