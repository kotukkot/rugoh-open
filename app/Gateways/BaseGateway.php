<?php

namespace App\Gateways;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BaseGateway
{
    private ?string $class;
    private bool $withTrashed = false;

    public function __construct(?string $class = null)
    {
        $this->class = $class;
    }

    /**
     * @return BaseGateway
     */
    public function withTrashed(): self
    {
        $this->withTrashed = true;

        return $this;
    }
}
