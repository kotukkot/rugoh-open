<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseModel
 * @package App
 */
class BaseModel extends Model
{
    /**
     * @return string
     */
    public static function getTableName(): string
    {
        return with(new static)->getTable();
    }


    /**
     * Save a new model and return the instance.
     *
     * @param array $attributes
     * @return Model|$this
     */
    public static function create(array $attributes = []): Model|null
    {
        $model = static::query()->create($attributes);

        return $model->id ? $model->fresh() : $model;
    }

    /**
     * Update the model in the database.
     * @param array $attributes
     * @param array $options
     * @return bool
     */
    public function update(array $attributes = [], array $options = []): bool
    {
        $this->setTable($this->getTableName());

        $updated = parent::update($attributes, $options);

        $this->refresh();

        return $updated;
    }

    /**
     * Delete the model from the database.
     * @return bool|null
     */
    public function delete(): ?bool
    {
        $this->setTable($this->getTableName());

        return parent::delete();
    }

    /**
     * @param DateTimeInterface $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date): string
    {
        return $date->format('Y-m-d H:i:s');
    }

    /**
     * @param string $alias
     * @param bool $withTrashed
     * @return EloquentBuilder
     */
    public static function alias(string $alias, bool $withTrashed = false): EloquentBuilder
    {
        $model = with(new static);

        $tableName = $model->getTable();

        $model->setTable($alias);

        if ($withTrashed) {
            $model = $model->withTrashed();
        }

        return $model->from("$tableName as $alias");
    }

}
