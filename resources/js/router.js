import { createRouter, createWebHistory } from 'vue-router';

import Home from './pages/Home/HomePage.vue';
import Events from './pages/Events/EventsPage.vue';
import Localizations from './pages/Localizations/LocalizationsPage.vue';

export const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/events',
            name: 'Events',
            component: Events
        },
        {
            path: '/localizations',
            name: 'Localizations',
            component: Localizations
        },

        { path: '/*', redirect: { name: 'Home' } },
    ]
});
