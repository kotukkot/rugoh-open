/**
 * Состояние бокового меню
 * открыто / закрыто
 */
export const sidebarIsClose = (state) => state.sidebarIsClose;

/**
 * Выбранная тема
 * светлая / тёмная
 */
export const isDarkTheme = (state) => state.isDarkTheme;

