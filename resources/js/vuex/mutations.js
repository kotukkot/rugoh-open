/**
 * Меняем состояние бокового меню
 * открыто / закрыто
 */
export const sidebarIsClose = (state, status) => {
    state.sidebarIsClose = status;
    localStorage.setItem('sidebarIsClose', JSON.stringify(status));
};

/**
 * Меняем тему
 * светлая / тёмная
 */
export const isDarkTheme = (state, status) => {
    state.isDarkTheme = status;
    localStorage.setItem('isDarkTheme', JSON.stringify(status));
};
